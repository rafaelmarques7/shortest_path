import os
import pprint
import random
import sys
import wx
import matplotlib
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
import numpy as np
import pylab


def loadData():
    vertices = [(ID, X, Y) for (ID, X, Y) in zip( \
        [i for i in range(1,11)], \
        [random.randint(0,10) for i in range(0,10)], \
        [random.randint(0,10) for i in range(0,10)])]
    #arcs = [()]

class MyFrame(wx.Frame):
    title = "Demo: Search Algorithm"

    def __init__(self):
        wx.Frame.__init__(self, None, -1, self.title)
        self.data = loadData()
