import wx
import matplotlib
matplotlib.use('WXAgg')
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.backends.backend_wxagg import \
FigureCanvasWxAgg as FigCanvas, \
NavigationToolbar2WxAgg as NavigationToolbar
from graph import Graph, RandomGraph
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import random

class Window(wx.Frame):
    title="Rafa's Shortest Path"
    num_vertex_list = ["5", "10", "20"]

    def __init__(self):
        #init parent
        wx.Frame.__init__(self, parent=None, id=-1, title=self.title, pos=(10,10), size=(720,480))
        #create a random graph
        #create main panel
        self.createMainPanel()
        #show the window
        self.Show(True)

    def createMainPanel(self):
        self.panel = wx.Panel(self, pos=(10,50), size=(480,320))
        #get number of vertex
        self.combo = wx.ComboBox(self.panel, choices=self.num_vertex_list,
            pos=(10, 20), value="Num. of Nodes", size=(120, 50))
        self.combo.Bind(wx.EVT_COMBOBOX, self.onVertexSelect)

    def onVertexSelect(self, event):
        #saves and acts on the user input
        self.num_nodes = int(self.combo.GetValue())
        self.draw()

    def draw(self):
        self.fig = plt.figure()
        self.canvas = FigCanvas(self.panel, -1, self.fig)
        self.Graph = RandomGraph(num_nodes = self.num_nodes)
        #define position of nodes (for internal representation)
        pos=nx.spring_layout(self.Graph.graph)
        #draw the graph; draw the edges labels
        nx.draw(self.Graph.graph, pos, node_size=500, labels=self.Graph.vertex_labels, with_labels=True)
        nx.draw_networkx_edge_labels(self.Graph.graph, pos, edge_labels = self.Graph.edge_labels)
        plt.axis('off')
        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.toolbar = NavigationToolbar(self.canvas)
        self.vbox.Add(self.toolbar, 0, wx.EXPAND)
        self.panel.SetSizer(self.vbox)
        self.vbox.Fit(self)


if __name__ == "__main__":
    app = wx.App()
    app.Frame = Window()
    app.MainLoop()
