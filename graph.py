import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import random

class Graph:
    """ A Graph is completly characterized by its adjacency matrix.
        It holds both the vertex and the edges.
        An edge exists if there is a positive value in the adjency matrix.
        That value is the cost of that edge.
        If the adjacency matrix is tri dimensional,
        it means that the edges cost vary with time.
    """
    def __init__(self, name="Graph", adj_matrix = None, labels = None):
        self.name = name
        self.adj_matrix = adj_matrix
        self.edges = []

    def randomGraph(self, complete=False, num_nodes=5):
        self.adj_matrix = self.createRandomAdjMatrix(complete, num_nodes)
        for index, weighth in np.ndenumerate(self.adj_matrix):
            self.edges.append((index[0], index[1], weighth))

    def createRandomAdjMatrix(self, complete, num_nodes):
        array = np.zeros([num_nodes, num_nodes])
        iterator = [(i,j) for i in range(0,num_nodes) for j in range(0, num_nodes)]
        for index in iterator:
            i, j = index[0], index[1]
            NUM_CONNECTIONS = 3
            if i == j:
                array[i][j] = 0
            else:
                if complete:
                    array[i][j] = random.randint(1,num_nodes + 1)
                else:
                    if random.randint(0,1) == 1:
                        array[i][j] = random.randint(1,num_nodes + 1)
                    else:
                        array[i][j] = 0
        return array

    def plotGraph(self):
        self.graph = nx.Graph()
        for edge in self.edges:
            #if weight != 0
            if edge[2]:
                self.graph.add_weighted_edges_from([edge])
        #label the vertex (names) and edges (weights)
        self.vertex_labels = dict((n, 'V' + str(index)) for index, n in enumerate(self.graph.nodes()))
        self.edge_labels = dict([((u, v,), w) for u,v,w in self.edges if w != 0 ])
        #define position of nodes (for internal representation)
        pos=nx.spring_layout(self.graph)
        #draw the graph; draw the edges labels
        nx.draw(self.graph, pos, node_size=500, labels=self.vertex_labels, with_labels=True)
        nx.draw_networkx_edge_labels(self.graph, pos, edge_labels = self.edge_labels)
        #plt.show()


class RandomGraph:
    """
        This class creates a random instance of a graph;
        internal variables: self.graph, self.vertex_labels, self.edge_labels,
                            self.edges, self.adj_matrix;
    """
    def __init__(self, num_nodes = 10, complete = False):
        self.getRandomGraph(num_nodes, complete)

    def getRandomGraph(self, num_nodes, complete):
        #create adjacency matrix
        self.adj_matrix = self.createRandomAdjMatrix(num_nodes, complete)
        #create weighted edges
        self.edges = []
        for index, weighth in np.ndenumerate(self.adj_matrix):
            self.edges.append((index[0], index[1], weighth))
        #create graph object and save it internally
        self.graph = nx.Graph()
        #add edges to graph
        for edge in self.edges:
            #if weight != 0
            if edge[2]:
                self.graph.add_weighted_edges_from([edge])
        #label the vertex (names) and edges (weights)
        self.vertex_labels = dict((n, 'V' + str(index)) for index, n in enumerate(self.graph.nodes()))
        self.edge_labels = dict([((u, v,), w) for u,v,w in self.edges if w != 0 ])

    def createRandomAdjMatrix(self, num_nodes, complete):
        array = np.zeros([num_nodes, num_nodes])
        iterator = [(i,j) for i in range(0,num_nodes) for j in range(0, num_nodes)]
        for index in iterator:
            i, j = index[0], index[1]
            NUM_CONNECTIONS = 3
            if i == j:
                array[i][j] = 0
            else:
                if complete:
                    array[i][j] = random.randint(1,num_nodes + 1)
                else:
                    if random.randint(0,1) == 1:
                        array[i][j] = random.randint(1,num_nodes + 1)
                    else:
                        array[i][j] = 0
        return array

if __name__ == "__main__":
    graph = Graph(name="myGraph")
    graph.randomGraph(complete=False, num_nodes = 7)
    graph.plotGraph()
