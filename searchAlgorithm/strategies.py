class BreadthFirst:
    def select(self, frontier):
        return frontier.pop(0)

    def expand(self, node_parent, problem):
        nodes_child = []
        for n, action in enumerate(problem.actions):
            state_parent = node_parent['state']
            state_new = problem.applyAction(state_parent, action)
            nodes_child.append({
                'parent': node_parent['id'],
                'action_executed': action,
                'id': node_parent['id'] + n + 1,
                'state': state_new,
                'cost': node_parent['cost'] + 1
            })
        return nodes_child

class DeapthFirst:
    def select(self, frontier):
        return frontier.pop()

    def expand(self, node_parent, problem):
        nodes_child = []
        for n, action in enumerate(problem.actions):
            state_parent = node_parent['state']
            state_new = problem.applyAction(state_parent, action)
            nodes_child.append({
                'parent': node_parent['id'],
                'action_executed': action,
                'id': node_parent['id'] + n + 1,
                'state': state_new,
                'cost': node_parent['cost'] + 1
            })
        return nodes_child

class UniformCost:
    #NOTE:
    #in case of state repetition (child state is already in frontier)
    #the path costs should be compared, and keep only the CHEAPER
    def select(self, frontier):
        #reorder the list according to cost
        frontier.sort(key = lambda x: x['cost'])
        return frontier.pop(0)

    def expand(self, node_parent, problem, frontier, explored):
        nodes_child = []
        for n, action in enumerate(problem.actions):
            state_parent = node_parent['state']
            state_new = problem.applyAction(state_parent, action)
            nodes_child.append({
                'parent': node_parent['id'],
                'action_executed': action,
                'id': node_parent['id'] + n + 1,
                'state': state_new,
                'cost': node_parent['cost'] + 1
            })
        frontier, explored = self.updateLists(nodes_child, frontier, explored)
        return frontier, explored


    def updateLists(self, nodes_child, frontier, explored):
        check_list = frontier + explored
        for n in check_list:
            for nn in nodes_child:
                if nn['state'] == n['state']:
                    #check cost
                    if nn['cost'] < n['cost']:
                        index = frontier.index(n)
                        frontier[index] = nn
                        nodes_child.remove(nn)
                    else:
                        nodes_child.remove(nn)
        for nn in nodes_child:
            frontier.append(nn)
        return frontier, explored

class AStar:
    def select(self, frontier):
        #reorder the list according to cost
        frontier.sort(key = lambda x: x['g_cost'])
        return frontier.pop(0)

    def expand(self, node_parent, problem, frontier, explored):
        nodes_child = []
        for n, action in enumerate(problem.actions):
            state_parent = node_parent['state']
            state_new = problem.applyAction(state_parent, action)
            g_cost = node_parent['g_cost'] + 1
            h_cost = problem.getHeuristicCost(state_new)
            f_cost = g_cost + h_cost
            nodes_child.append({
                'parent': node_parent['id'],
                'action_executed': action,
                'id': node_parent['id'] + n + 1,
                'state': state_new,
                'g_cost': g_cost,
                'h_cost': h_cost,
                'f_cost': f_cost
            })
        frontier, explored = self.updateLists(nodes_child, frontier, explored)
        return frontier, explored

    def updateLists(self, nodes_child, frontier, explored):
        """
            if child.State not in frontier or explored:
                frontier.insert(child)
            elif:
                child.state is in frontier with higher cost:
                    replace frontier node with child
        """
        check_list = frontier + explored
        for new_node in nodes_child:
            in_check_list = self.verifyCheckList(new_node, check_list)
            #if not in frontier or explored, add to frontier
            if not in_check_list:
                frontier.append(new_node)
            #else, check frontier, replace if current price is higher
            else:
                self.verifyFrontierAndUpdate(new_node, frontier)
        return frontier, explored

    def verifyFrontierAndUpdate(self, new_node, frontier):
        for node in frontier:
            #verifies if new_node is already in frontier
            if new_node['state'] == node['state']:
                #replaces if new_node cost is lower than the current
                if new_node['f_cost'] < node['f_cost']:
                    index = frontier.index(node)
                    frontier[index] = new_node
                break

    def verifyCheckList(self, new_node, check_list):
        for node in check_list:
            if new_node['state'] == node['state']:
                return True
        return False
