from random import shuffle
from copy import deepcopy

class EightTile:
    """
        the board is defined by a list of 3 lists of 3 elements each.
        y represents the (big) list position, x the position inside that list
         -------> x (0,1,2)
        | 1 0 3                 => (x, y) = (1, 0)
        |
        |
     y  v

    """
    def __init__(self):
        self.state_goal = self.getStateGoal()
        self.state_init = self.getInitState()
        self.actions = self.getActions()

    def checkGoal(self, node, explored):
        if node['state'] == self.getStateGoal():
            self.displaySolutionAndExit(node, explored)

    def getStateGoal(self):
        return  [ [0, 1, 2], [3, 4, 5], [6, 7, 8] ]

    def displaySolutionAndExit(self, node_sel, explored):
        solution, node = [], node_sel
        while True:
            solution.append(node)
            id_parent = node['parent']
            if id_parent == 0:
                break
            for n in explored:
                if n['id'] == id_parent:
                    node = n
                    break
        solution = sorted(solution, key = lambda x: x['parent'])
        print('____________________________________________________________________________________________________________________')
        print('\n               SOLUTION:\n')
        for item in solution:
            print(item)
        print('Solution cost: ')
        try:
            print(node_sel['cost'])
        except:
            pass
        try:
            print(node_sel['g_cost'])
        except:
            pass
        print('____________________________________________________________________________________________________________________')
        exit()

    def getInitState(self):
        l = [i for i in range(0,9)]
        shuffle(l)
        state_init = [ [],[],[] ]
        for i in range(0,3):
            for j in range(0,3):
                state_init[i].append(l.pop())
        return state_init
        #return  [ [3, 1, 2], [6, 4, 5], [0, 7, 8] ]

    def getActions(self):
        return ["up", "down", "right", "left"]

    def applyAction(self, state, action):
        #we can verify the position of the blank tile, in order to
        #check if the operation can be executed,
        #or if we return the original state (which means, it should not be added to child_nodes)
        (x, y) = self.getBlankPosition(state)
        if action == "up":
            return self.applyUp(state, x, y)
        if action == "down":
            return self.applyDown(state, x, y)
        if action == "right":
            return self.applyRight(state, x, y)
        if action == "left":
            return self.applyLeft(state, x, y)
        else:
            print('WARNING: Action not recognised')
            return None

    def getBlankPosition(self, state):
        blank = 0
        x, y = 0, 0
        for i in range(0,3):
            if blank in state[i]:
                y = i
        for j in range(0,3):
            if blank == state[y][j]:
                x = j
        return (x, y)

    def applyUp(self, state, x, y):
        if y == 0:
            return state
        state_new = deepcopy(state)
        item_top, item_bot = state_new[y-1][x], state_new[y][x]
        state_new[y-1][x], state_new[y][x] = item_bot, item_top
        return state_new

    def applyDown(self, state, x, y):
        if y == 2:
            return state
        state_new = deepcopy(state)
        item_top, item_bot = state_new[y][x], state_new[y+1][x]
        state_new[y][x], state_new[y+1][x] = item_bot, item_top
        return state_new

    def applyRight(self, state, x, y):
        if x == 2:
            return state
        state_new = deepcopy(state)
        item_left, item_right = state_new[y][x], state_new[y][x+1]
        state_new[y][x], state_new[y][x+1] = item_right, item_left
        return state_new

    def applyLeft(self, state, x, y):
        if x == 0:
            return state
        state_new = deepcopy(state)
        item_left, item_right = state_new[y][x-1], state_new[y][x]
        state_new[y][x-1], state_new[y][x] = item_right, item_left
        return state_new

    def wrongTiles(self, state):
        correct_state = [i for i in range(0,9)]
        curr_state = [i for sub_list in state for i in sub_list]
        wrong_tiles = 0
        for x, y in zip(curr_state, correct_state):
            if x != y:
                wrong_tiles += 1
        return wrong_tiles

    def manhattanDistance(self, state):
        #md = manhattanDistance
        md = 0
        state_goal = self.getStateGoal()
        zero, one, two = [0,0], [0,1], [0,2]
        three, four, five = [1,0], [1,1], [1,2]
        six, seven, eight = [2,0], [2,1], [2,2]
        #iterate over state
        for index_outter, sub_list in enumerate(state):
            for index_inner, item in enumerate(sub_list):
                #compare curr tile to goal tile
                correct_value = state_goal[index_outter][index_inner]
                current_value = state[index_outter][index_inner]
                if current_value != correct_value:
                    #find the correct position of that tile
                        correct_outter_index, correct_inner_index = self.getCorrectPosition(state_goal, current_value)
                        md += abs(index_outter - correct_outter_index) + abs(index_inner - correct_inner_index)
        return md

    def getCorrectPosition(self, state_goal, value):
        for o in range(0,3):
            for i in range(0,3):
                if value == state_goal[o][i]:
                    return (o, i)

    def getHeuristicCost(self, state):
        #we will use the manhattanDistance
        #there are, at least, two possible heuristics for the eighTtile
        #1) number of missplaced tiles
        wrong_tiles = self.wrongTiles(state)
        #-> 2) (manhatten) distances of each tile to its correct position
        manhattan_distance = self.manhattanDistance(state)
        #return manhattan_distance
        return wrong_tiles
