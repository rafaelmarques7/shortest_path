#a Graph is a search space in which nodes CAN be repeated
from models import EightTile
from strategies import BreadthFirst, DeapthFirst, UniformCost

def grapSearch(problem, strategy):
    "returns solution or failure"
    frontier = initialise(problem)
    explored = []
    n = 0
    while True:
        print(n)
        if not frontier:
            print("FAILURE\nFrontier is empty")
            break
        #select a node and remove from frontier
        node_sel = applySelectionStrategy(frontier, strategy)
        #check for goal state and return solution if goal state
        if checkGoalState(node_sel, problem):
            getSolution(node_sel, explored)
            break
        explored.append(node_sel)
        #expand the nodes and add to frontier
        applyExpandStrategy(node_sel, frontier, explored, strategy, problem)
        n += 1

def initialise(problem):
    "initialise the frontier with the initial state of the problem"
    state_init = problem.getInitState()
    node_init = {
        'parent': 0,
        'id': 1,
        'action': None,
        'state': state_init,
        'cost': 0
    }
    frontier = [node_init]
    return frontier

def applySelectionStrategy(frontier, strategy):
    "selects and removes a node from the frontier"
    return strategy.select(frontier)

def checkGoalState(node_sel, problem):
    "verifies if the selected node is a goal state"
    return node_sel['state'] == problem.getStateGoal()

def applyExpandStrategy(node_sel, frontier, explored, strategy, problem):
    "aplies the selected strategy to expand the frontier based on the selected node"
    #if node_sel in explored:
    #    return None
    if strategy.__class__.__name__ == 'UniformCost':
        frontier, explored = strategy.expand(node_sel, problem, frontier, explored)
    else:
        nodes_new = strategy.expand(node_sel, problem)
        check_list = frontier + explored
        #check if state of each child node is in any of the two lists (open and explored)
        #if yes: remove child node from list
        #in the end, add the remaining child nodes to frontier
        for n in check_list:
            for nn in nodes_new:
                if nn['state'] == n['state']:
                    nodes_new.remove(nn)
        for n in nodes_new:
            frontier.append(n)

def getSolution(node_sel, explored):
    solution = []
    node = node_sel
    while True:
        solution.append(node)
        id_parent = node['parent']
        if id_parent == 0:
            break
        for n in explored:
            if n['id'] == id_parent:
                node = n
                break
    print('Solution cost: ')
    print(node_sel['cost'])
    print('SOLUTION:\n')
    for item in solution:
        print(item)
        print('\n')

if __name__ == "__main__":
    problem = EightTile()
    #strategy = BreadthFirst()
    #strategy = DeapthFirst()
    strategy = UniformCost()
    #print(strategy.__class__.__name__) returns the name of the strategy
    grapSearch(problem, strategy)
